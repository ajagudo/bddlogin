//
//  LoginPresenter.swift
//  BDDLoginPrueba
//
//  Created by Alejandro Jiménez Agudo on 19/1/17.
//  Copyright © 2017 Alesete. All rights reserved.
//

import Foundation


protocol LoginView {
	func show(message: String)
}

struct LoginPresenter {
	
	let loginView: LoginView
	let loginInteractor: LoginInteractor
	
	func userDidLogin(email: String, password: String) {
		self.loginInteractor.login(email: email, password: password) { result in
			switch result {
			case .success(_):
				self.loginView.show(message: "SUCCESS")
				
			case .error(let message):
				self.loginView.show(message: message)
			}
		}
	}
	
}
