//
//  LoginInteractor.swift
//  BDDLoginPrueba
//
//  Created by Alejandro Jiménez Agudo on 19/1/17.
//  Copyright © 2017 Alesete. All rights reserved.
//

import Foundation
import GIGLibrary

struct LoginInteractor {
	
	let service: LoginService
	
	func login(email: String, password: String, completion: @escaping (Result<Bool, String>) -> Void ) {
		guard !email.isEmpty else {
			completion(.error("Email vacio"))
			return
		}
		
		self.service.login(email: email, password: password) { result in
			switch result {
			case .success(_):
				completion(.success(true))
				
			case .error(let message):
				completion(.error(message))
			}
		}
	}
	
}
