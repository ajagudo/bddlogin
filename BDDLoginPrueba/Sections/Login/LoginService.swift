//
//  LoginService.swift
//  BDDLoginPrueba
//
//  Created by Alejandro Jiménez Agudo on 19/1/17.
//  Copyright © 2017 Alesete. All rights reserved.
//

import Foundation
import GIGLibrary


struct LoginService {
	
	func login(email: String, password: String, completion: @escaping (Result<Bool, String>) -> Void) {
		let request = Request(
			method: "POST",
			baseUrl: "https://test",
			endpoint: "/login",
			headers: ["Content-Type":"application/json"],
			bodyParams: [
				"email": email,
				"password": password
			],
			verbose: true
		)
		
		request.fetch { response in
			switch response.status {
			case .success:
				completion(.success(true))
				
			default:
				completion(.error("Usuario incorrecto"))
			}
		}
	}
	
}
