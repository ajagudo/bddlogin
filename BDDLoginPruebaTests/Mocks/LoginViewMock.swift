//
//  LoginViewMock.swift
//  BDDLoginPrueba
//
//  Created by Alejandro Jiménez Agudo on 19/1/17.
//  Copyright © 2017 Alesete. All rights reserved.
//

import Foundation
import XCTest
@testable import BDDLoginPrueba

class LoginViewMock: LoginView {
	
	var spyShow = (called: false, message: "")
	var expectation: XCTestExpectation?
	
	func show(message: String) {
		self.spyShow.called = true
		self.spyShow.message = message
		self.expectation?.fulfill()
	}
	
}
