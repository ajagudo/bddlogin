//
//  LoginTests.swift
//  BDDLoginPrueba
//
//  Created by Alejandro Jiménez Agudo on 19/1/17.
//  Copyright © 2017 Alesete. All rights reserved.
//

import XCTest
import OHHTTPStubs
import GIGLibrary
@testable import BDDLoginPrueba

class LoginTests: XCTestCase {
	
	var loginPresenter: LoginPresenter!
	var loginViewMock: LoginViewMock!
    
    override func setUp() {
        super.setUp()
		
		self.loginViewMock = LoginViewMock()
		self.loginPresenter = LoginPresenter(
			loginView: self.loginViewMock,
			loginInteractor: LoginInteractor(
				service: LoginService()
			)
		)
    }
    
    override func tearDown() {
		self.loginViewMock = nil
		self.loginPresenter = nil
		OHHTTPStubs.removeAllStubs()
		
        super.tearDown()
    }
    
    func test_not_nil() {
        XCTAssertNotNil(self.loginPresenter)
    }
	
	func test_loginPresenter_showSuccessInView_whenAPIisSuccess() {
		// ARRANGE
		self.loginViewMock.expectation = self.expectation(description: "api returns")
		let _ = stub(condition: isPath("/login")) { _ in
			return StubResponse.stubResponse(with: "login_ok.json")
		}
		
		// ACT
		self.loginPresenter.userDidLogin(email: "test@gigigo.com", password: "12345")
		
		//ASSERT
		self.waitForExpectations(timeout: 1, handler: nil)
		XCTAssert(self.loginViewMock.spyShow.called == true)
		XCTAssert(self.loginViewMock.spyShow.message == "SUCCESS")
	}
	
	func test_loginPresenter_setParamsToAPI() {
		// ARRANGE
		self.loginViewMock.expectation = self.expectation(description: "api returns")
		OHHTTPStubs.stubRequests(passingTest: { request -> Bool in
			let data = (request as NSURLRequest).ohhttpStubs_HTTPBody()
			let json = try! JSON.dataToJson(data!)
			
			XCTAssert(json["email"]?.toString() == "test@gigigo.com")
			XCTAssert(json["password"]?.toString() == "12345")
			return true
		}) { _ in
			return StubResponse.stubResponse(with: "login_ok.json")
		}
		
		// ACT
		self.loginPresenter.userDidLogin(email: "test@gigigo.com", password: "12345")
		
		//ASSERT
		self.waitForExpectations(timeout: 1, handler: nil)
	}
	
	func test_loginPresenter_showWrongUser_whenAPIFailed() {
		// ARRANGE
		self.loginViewMock.expectation = self.expectation(description: "api returns")
		let _ = stub(condition: isPath("/login")) { _ in
			return StubResponse.stubResponse(with: "login_ko.json")
		}
		
		// ACT
		self.loginPresenter.userDidLogin(email: "test@gigigo.com", password: "12345")
		
		//ASSERT
		self.waitForExpectations(timeout: 1, handler: nil)
		XCTAssert(self.loginViewMock.spyShow.called == true)
		XCTAssert(self.loginViewMock.spyShow.message == "Usuario incorrecto")
	}
	
	func test_loginPresenter_showEmptyMail_whenMailIsEmpty() {
		// ARRANGE
		
		// ACT
		self.loginPresenter.userDidLogin(email: "", password: "12345")
		
		//ASSERT
		XCTAssert(self.loginViewMock.spyShow.called == true)
		XCTAssert(self.loginViewMock.spyShow.message == "Email vacio")
	}
}
